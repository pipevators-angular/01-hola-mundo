import {Component} from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html'
})
export class BodyComponent {
  mostrar: boolean = true;

  frase: any = {
      mensaje: 'un gran poder requierre una gran responsabilidad',
      autor: 'Peter Parker'
  };

  personajes: string[] = ['SpiderMan','Venom', 'Carnage']
}
